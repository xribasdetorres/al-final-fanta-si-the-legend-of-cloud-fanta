grav = 0.5;
hsp = 0;
vsp = 0;

key_jump_dir = 0;
movespeed = 6;

jumpspeed_normal = 6;
jumpspeed_powerup = 10;
PowerUpPistola = false;


jumpspeed = jumpspeed_normal;
hcollision = false;
fearofheights = false;
image_speed = 0.3;

//Constants
grounded = false;
jumping = false;

//Horizontal jump constants
hsp_jump_constant_small = 1;
hsp_jump_constant_big = 4;
hsp_jump_applied = 0;

//Horizontal key pressed count
hkp_count = 0;
hkp_count_small = 2;
hkp_count_big = 5;

//Init variables
key_left = 0;
key_right = 0;
key_jump = false;

//sprite perssonaje
dirrecion = 0;

//Datos Personaje
Vidas = 100;
global.PowerUpArma = "Ninguno";
global.PowerUpEscudo = false;

//globales -> Da datos para el hud
global.Vida = Vidas;
