move = key_left + key_right;
hsp = move * movespeed
global.Vida = Vidas;

if(vsp<10){
     vsp+=grav;
};

if(place_meeting(x,y+1,obj_suelo))
{
    if(!grounded && !key_jump)
    {
        hkp_count = 0;
        jumping = false;
    }else if(grounded && key_jump)
    {
        jumping = true;
    }
        
        grounded = !key_jump;
        
        vsp = key_jump * -jumpspeed;

}

if(grounded)
{
    hsp_jump_applied = 0;
    
}

if(move!=0 && grounded)
{
    hkp_count++;
}else if(move==0 && grounded){
    hkp_count=0;
}

if(jumping)
{
     if(hsp_jump_applied == 0){
        hsp_jump_applied = sign(move);       
    }


    if(hkp_count < hkp_count_small )
    {
        hsp = 0;
    }else if (hkp_count >= hkp_count_small && hkp_count < hkp_count_big)
    {
        hsp = hsp_jump_applied * hsp_jump_constant_small;
    }else{
        hsp = hsp_jump_applied * hsp_jump_constant_big;
    }
}

if(place_meeting(x+hsp, y, obj_pared) or place_meeting(x+hsp, y, obj_suelo))
{
    while(!place_meeting(x+sign(hsp), y, obj_pared) and !place_meeting(x +sign(hsp), y, obj_suelo))
    {
        x += sign(hsp);
    }
        hsp = 0;
        hcollision = true; 
}

x += hsp;

if(place_meeting(x, y+vsp, obj_suelo))
{
    while(!place_meeting(x, y + sign(vsp) ,obj_suelo))
    {
        y+= sign(vsp);
    }
    vsp = 0;
}    

y += vsp;

if(place_meeting(x,y+8,obj_suelo))
{  
    if(fearofheights && 
       !position_meeting(x+(sprite_width/2)*dir,y+(sprite_height/2)+8,obj_wall))
    {
        dir *= -1;
    }
}
